# Zetetika @ Matematčki fakultet #

## Literatura

Šifra za literaturu: zetetika

## Uputstvo za kačenje radova

- U direktorjum `seminarski_2021` smestite svoj direktorijum i nazovite ga `rbr_NazivTeme`
- Na primer, ako je Vaš seminarski pod rednim brojem 17 u zajedničkoj tabeli i tema Vam se zove Rotirajući krugovi, onda bi folder trebalo da se zove `17_RotirajuciKrugovi`
- Da biste mogli bilo šta da okačite na server, potrebno je da upišete svoje korisničko ime u našu zajedničku tabelu, kako bih Vas ubacila u grupu korisnika koja ima odgovarajuće privilegije.

## Rokovi

Videti na [http://poincare.matf.bg.ac.rs/~danijela/ga.html](http://poincare.matf.bg.ac.rs/~danijela/ga.html)


## Korišćenje git-a

- Napraviti nalog na [bitbucket-u](https://bitbucket.org)

- Instalirati git:

	- Za Ubuntu, u terminalu: `sudo apt install git`
	- [Git za Windows](https://git-scm.com/download/win) (komande su iste kao i u linux sistemima) 

- [Osnovne git komande](https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html)

- Preuzimanje repozitorijuma: `git clone https://bitbucket.org/zetetika/seminarski.git`

- Povezivanje sa Atlassian nalogom:

	- git config user.name "korisničko ime iz tabele"
	- git config user.email "povezani mejl iz tabele"

- Tok rada:

	- Preuzeti nove izmene: `git pull`
	- Napraviti svoje izmene
	- Nakon napravljenih izmena, izmene se mogu podići na repozitorijum:

```
git add <files>
git commit -m 'opis izmena'
git push
```
	- Ukoliko je potrebno ukucati svoje korisnicko ime i šifru

- Jos neke korisne i često korišćene naredbe:
	- git log
	- git diff
	- git show _broj_izmena_


## Prikaz seminarskog

Materijal je u vidu:

- slajdova (15-20, LaTeX - beamer)
- obavezno predati pdf i tex

Radovi ce biti ocenjeni nakon odbrane.

Jedan od ciljeva ovog projekta da u izabranom radu uočite šta je najbitnite i da to prikažete i istaknete.

Držati se preporučenog obima materijala, prekoračenja u bilo kom smeru smatraju se propustom. 

U direktorijumu `demo` se nalazi krako uputsvo za korišćenje `tex`-a i pravljenje prezentacija.